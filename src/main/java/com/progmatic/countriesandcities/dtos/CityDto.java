/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.countriesandcities.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author peti
 */
public class CityDto {
    
    private String id;
    
    private String name;
    
    private Long population;    
    
    private CountryDto country;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPopulation() {
        return population;
    }

    public void setPopulation(Long population) {
        this.population = population;
    }

    @JsonIgnore
    public CountryDto getCountry() {
        return country;
    }

    @JsonProperty
    public void setCountry(CountryDto country) {
        this.country = country;
    }
    
    
}
